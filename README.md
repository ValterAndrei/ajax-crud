# Aplicativo simples utilizando o Docker Compose

1. Criar e inserir conteúdo no arquivo Dockerfile

```dockerfile
  FROM ruby:2.3.3
  RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
  RUN mkdir /blog
  WORKDIR /blog
  ADD Gemfile /blog/Gemfile
  ADD Gemfile.lock /blog/Gemfile.lock
  RUN bundle install
  ADD . /blog
```

2. Criar arquivo Gemfile (será sobrescrito)

```
  source 'https://rubygems.org'
  gem 'rails', '5.0.0.1'
```

3. Criar arquivo Gemfile.lock

```
$ touch Gemfile.lock
```

4. Criar e inserir conteúdo no arquivo docker-compose.yml

```
version: '3'
services:
  db:
    image: postgres
  web:
    build: .
    command: bundle exec rails s -p 3000 -b '0.0.0.0'
    volumes:
      - .:/blog
    ports:
      - "3000:3000"
    depends_on:
      - db
```

5. Construir o projeto

```
$ docker-compose run web rails new . --force --database=postgresql
```

6. Se for linux executar este comando

```
$ sudo chown -R $USER:$USER .
```

7. Construindo a imagem

```
$ docker-compose build
```

8. Editando arquivo `config/database.yml`

```
default: &default
  adapter: postgresql
  encoding: unicode
  host: db
  username: postgres
  password:
  pool: 5

development:
  <<: *default
  database: blog_development


test:
  <<: *default
  database: blog_test
```

9. Subindo aplicação

```
$ docker-compose up
```

10. Execute este comando em outro terminal

```
$ docker-compose run web rake db:create
```

11. Acesse a página localhost

`
http://localhost:3000
`
